package recfun

object count {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(308); 
 def countChange(money: Int, coins: List[Int]): Int = {
 //println(money + ":" + coins.length)
  if (money == 0) 1
  	else if (money < 0 || coins.length == 0) 0
  	else countChange(money, coins.tail) + countChange(money-coins.head, coins)
  	
  	//countChange(money, coins)
  };System.out.println("""countChange: (money: Int, coins: List[Int])Int""");$skip(26); val res$0 = 
 countChange(4,List(1,2));System.out.println("""res0: Int = """ + $show(res$0));$skip(47); val res$1 = 
 countChange(300,List(5,10,20,50,100,200,500));System.out.println("""res1: Int = """ + $show(res$1));$skip(47); val res$2 = 
 countChange(301,List(5,10,20,50,100,200,500));System.out.println("""res2: Int = """ + $show(res$2));$skip(47); val res$3 = 
 countChange(300,List(500,5,50,100,20,200,10));System.out.println("""res3: Int = """ + $show(res$3))}
 
 
}
