package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }
 
  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
   
    if(c == 0 || r == c)
      return 1
      else
      return pascal(c-1,r-1)+pascal(c,r-1)
      
      
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def count(counter:Int, chars:List[Char]): Int = {
      if(counter < 0 || chars.length == 0) counter
      else{
        count({
          if (chars.head == '(') counter + 1
          else if (chars.head == ')') counter - 1 
          else counter
        },chars.tail)
      }
      
      
    }
    count(0,chars) == 0
  }

  /**
   * Exercise 3
   */
  
    def countChange(money: Int, coins: List[Int]): Int = {

	  if (money == 0) 1
	  else if (money < 0 || coins.length == 0) 0
	  else countChange(money, coins.tail) + countChange(money-coins.head, coins)
	  	
    }  
    
}

